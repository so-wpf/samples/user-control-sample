﻿using System.Windows;

namespace UserControlSample
{
    public partial class TemperatureUserControl
    {
        public static readonly DependencyProperty UnitProperty = DependencyProperty.Register(
            nameof(Unit), typeof(ValidUnits), typeof(TemperatureUserControl), new PropertyMetadata(ValidUnits.C, OnTemperatureUnitChange));

        static void OnTemperatureUnitChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var value = (double)dependencyObject.GetValue(ValueProperty);

            if ((ValidUnits)dependencyPropertyChangedEventArgs.NewValue == ValidUnits.C)
                dependencyObject.SetValue(ValueProperty, (value - 32) / 1.8);
            else
                dependencyObject.SetValue(ValueProperty, value * 1.8 + 32);
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value), typeof(double), typeof(TemperatureUserControl), new PropertyMetadata(24d));

        public TemperatureUserControl() => InitializeComponent();

        public ValidUnits Unit
        {
            get => (ValidUnits)GetValue(UnitProperty);
            set => SetValue(UnitProperty, value);
        }

        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }


    }

    public enum ValidUnits : byte
    {
        C,
        F
    }
}
